package com.document;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.List;
import java.util.ArrayList;

public class VersionDAO {
	
	private String jdbcURL;
	private String jdbcUsername;
	private String jdbcPassword;
	private Connection jdbcConnection;
	
	public VersionDAO(String jdbcURL, String jdbcUsername, String jdbcPassword) {
		this.jdbcURL = jdbcURL;
		this.jdbcUsername = jdbcUsername;
		this.jdbcPassword = jdbcPassword;
	}
	
	public void connect()throws SQLException {
        if (jdbcConnection == null || jdbcConnection.isClosed()) {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new SQLException(e);
            }
            jdbcConnection = DriverManager.getConnection(
                                        jdbcURL, jdbcUsername, jdbcPassword);
        }
    }
     
    protected void disconnect() throws SQLException {
        if (jdbcConnection != null && !jdbcConnection.isClosed()) {
            jdbcConnection.close();
        }
    }
     
    public boolean saveVersionHistory(VersionFiles versionFiles) throws SQLException {
    	
    	boolean status = false;
    	PreparedStatement statement = null ;
    	int rowsInserted = 0;
    	System.out.println(" ===========> IN VersionDAO - saveDocument ");
        String SQL_DOCUMENT_INSERT = "INSERT INTO document (name, created_on, created_by) VALUES (?, ?, ?)";
        String SQL_VERSION_INSERT = "INSERT INTO version (version_name, document_id, created_on, created_by) VALUES (?, ?, ?, ?)";
        connect();
        try{ 
        	
	        statement = jdbcConnection.prepareStatement(SQL_DOCUMENT_INSERT,Statement.RETURN_GENERATED_KEYS);
	        statement.setString(1, versionFiles.getDocument());
	        statement.setTimestamp(2, new java.sql.Timestamp(versionFiles.getCreatedDate().getTime()));
	        statement.setString(3, versionFiles.getCreatedBy());
	                
	        rowsInserted = statement.executeUpdate();
	        
	        if(rowsInserted  ==0){
	        	throw new SQLException(" Adding Document failed");
	        }
	        
	        ResultSet generatedKeys = statement.getGeneratedKeys();
	        if(generatedKeys.next()){
	        	versionFiles.setDocument_id((int)generatedKeys.getLong(1));
	        	 statement = jdbcConnection.prepareStatement(SQL_VERSION_INSERT,Statement.RETURN_GENERATED_KEYS);
	        	 statement.setString(1, versionFiles.getVersion());
	 	         statement.setInt(2, versionFiles.getDocument_id());
	 	         statement.setTimestamp(3, new java.sql.Timestamp(versionFiles.getCreatedDate().getTime()));
	 	         statement.setString(4, versionFiles.getCreatedBy());
	 	          	         
	 	        rowsInserted = statement.executeUpdate();
	 	        System.out.println(" ===========> IN VersionDAO - saveDocument - rowInserted - VERSION TABLE "+rowsInserted);
	 	        if(rowsInserted  ==0){
	 	        	throw new SQLException(" Adding Version failed");
	 	        }
	        }
	        else{
	        	throw new SQLException(" Adding Document failed");
	        }
        }catch(SQLException e){
        	e.printStackTrace();
        }finally{
            statement.close();
            disconnect();
        }
        return status;
    }
    
    public List<VersionFiles> getVersionHistory()throws SQLException{
    	List<VersionFiles> versionHistory = new ArrayList<VersionFiles>();
    	System.out.println("IN getVersionHistory");
    	
    	connect();
    	
    	String sql = "select d.name, v.version_name, v.created_on, v.created_by from "+
    					"document d RIGHT JOIN version v on d.document_id = v.document_id "+
    					"order by d.created_on"	;
    	Statement statement = jdbcConnection.createStatement();
    	ResultSet resultSet = statement.executeQuery(sql);
    	
    	while(resultSet.next()){
    		String docName = resultSet.getString("d.name");
    		String versionName = resultSet.getString("v.version_name");
    		Timestamp d = resultSet.getTimestamp("v.created_on");
    		String created_by = resultSet.getString("v.created_by");
    		    		
    		VersionFiles versionfile = new VersionFiles();
    		versionfile.setDocument(docName);
    		versionfile.setVersion(versionName);
    		versionfile.setCreatedDate(new Date(d.getTime()));
    		versionfile.setCreatedBy(created_by);

    		versionHistory.add(versionfile);
    	}	
    		resultSet.close();
            statement.close();
             
            disconnect();
    	
    		
    	return versionHistory;
    }
    
    
}

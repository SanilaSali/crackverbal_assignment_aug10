package com.document;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ServiceLoader;

import javax.jcr.Binary;
import javax.jcr.GuestCredentials;
import javax.jcr.Node;
import javax.jcr.NodeIterator;
import javax.jcr.PathNotFoundException;
import javax.jcr.Property;
import javax.jcr.PropertyIterator;
import javax.jcr.Repository;
import javax.jcr.RepositoryException;
import javax.jcr.RepositoryFactory;
import javax.jcr.Session;
import javax.jcr.SimpleCredentials;
import javax.jcr.Value;
import javax.jcr.ValueFactory;
import javax.jcr.query.InvalidQueryException;
import javax.jcr.query.Query;
import javax.jcr.query.QueryManager;
import javax.jcr.query.QueryResult;
import javax.jcr.version.Version;
import javax.jcr.version.VersionHistory;
import javax.jcr.version.VersionIterator;
import javax.jcr.version.VersionManager;
import javax.naming.Context;
import javax.naming.InitialContext;

import org.apache.commons.io.IOUtils;
import org.apache.jackrabbit.JcrConstants;
import org.apache.jackrabbit.commons.JcrUtils;
import org.apache.jackrabbit.core.TransientRepository;
import org.apache.jackrabbit.core.data.LazyFileInputStream;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.annotation.WebServlet;

import org.apache.poi.xwpf.extractor.XWPFWordExtractor;
import org.apache.poi.xwpf.usermodel.VerticalAlign;
import org.apache.poi.xwpf.usermodel.XWPFDocument;
import org.apache.poi.xwpf.usermodel.XWPFParagraph;
import org.apache.poi.xwpf.usermodel.XWPFRun;

import com.document.VersionFiles;
import com.document.VersionDAO;


/**
 * Servlet implementation class DocumentServlet
 */

public class DocumentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DocumentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }
    
    private static List<VersionFiles> versionFiles;
    private static VersionManager vManager;
    private static Session session = null;
    private static Node fileNode;
    private static Node root;
    private static Node fileNodeContent;
    private static VersionDAO versionDAO;
    public static boolean rootDocument = true;
    
    public void init() {
        String jdbcURL = getServletContext().getInitParameter("jdbcURL");
        String jdbcUsername = getServletContext().getInitParameter("jdbcUsername");
        String jdbcPassword = getServletContext().getInitParameter("jdbcPassword");
        try {
        	versionDAO = new VersionDAO(jdbcURL, jdbcUsername, jdbcPassword);
        	Repository repository = JcrUtils.getRepository();
        	session = repository.login(new SimpleCredentials("admin", "admin".toCharArray()));
 			String user = session.getUserID(); 
            String name = repository.getDescriptor(Repository.REP_NAME_DESC); 
            System.out.println("Logged in as " + user + " to a " + name + " repository."); 
            root = session.getRootNode();
 			vManager = session.getWorkspace().getVersionManager();
 		}catch(Exception e){
 		} 
        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("*********************** IN doGET ****************************");
		try{
		HttpSession httpsession = request.getSession();
        String versionToOpen = request.getParameter("param1");
        String docToOpen = request.getParameter("param2");
		System.out.println(" versionToOpen = "+versionToOpen);
		System.out.println(" docToOpen = "+docToOpen);
		List<VersionFiles> versionHistory = versionDAO.getVersionHistory();
		//OPEN DOCUMENT
		String docContent = viewDocument(docToOpen,versionToOpen);
		if(null!=versionToOpen){
			httpsession.setAttribute("docContent", docContent);
			httpsession.setAttribute("versionToOpen", versionToOpen);
			httpsession.setAttribute("docToOpen",docToOpen);
		}
		else{
				httpsession.setAttribute("docContent", "");
				httpsession.setAttribute("versionToOpen", "");
		}
		request.setAttribute("versionHistory",versionHistory);
		RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
        dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("*********************** IN doPOST ****************************");
		HttpSession httpsession = request.getSession();
       
		String content = request.getParameter("mydoc");
		String docName = request.getParameter("docName");
		String version = (String)httpsession.getAttribute("versionToOpen");
		System.out.println(" Version clicked = "+version);
		String action = request.getServletPath();
		FileOutputStream out =null;
		
		try {
			   //create Document and Version
		      if(version.equals("")){
		    	  XWPFDocument document = new XWPFDocument(); 
				  String fileName = "D:\\Sanila\\CrackVerbal\\"+docName;
			      out = new FileOutputStream(new File(fileName));
			      XWPFParagraph paragraph = document.createParagraph();
			      XWPFRun run = paragraph.createRun();
			      run.setText(content);
			      XWPFRun paragraphOneRunThree = paragraph.createRun();
			      paragraphOneRunThree.setFontSize(30);
			      document.write(out);
			      System.out.println("Document created successfully");
			  	  createDocumentVersion(docName);
			  	
		      }
		      else{
		    	  docName = (String) httpsession.getAttribute("docToOpen");
		    	  editDocument(docName,content);
	      }
		    versionFiles = addToVersionHistory(docName,fileNode.getPath());
		    List<VersionFiles> versionHistory = versionDAO.getVersionHistory();
		    request.setAttribute("versionHistory",versionHistory);
		    httpsession.setAttribute("docContent", "");
			httpsession.setAttribute("versionToOpen", "");
		    RequestDispatcher dispatcher = request.getRequestDispatcher("home.jsp");
	        dispatcher.forward(request, response);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
 	}
 
	public static void createDocumentVersion(String docName) throws Exception {
		System.out.println("IN createDocumentVersion rootDocument"+rootDocument);
		String path= null;
		try {
			if(rootDocument)
				path = addVersioning(docName, root);
			else 
				path  = addVersioning(docName, fileNode);
		}catch(Exception e){
		} 
	}

	public static String addVersioning(String docName, Node parentNode) throws RepositoryException {
		System.out.println("IN addVersioning docName and parentNode are :" +docName+ "," +parentNode);	
		//create nt:file node
		String fileName = "D:\\Sanila\\CrackVerbal\\"+docName;
		File file = new File(fileName);   
		FileInputStream is;
		
		try {
			is = new FileInputStream(file);
			Calendar lastModified = Calendar.getInstance();   
			lastModified.setTimeInMillis(lastModified.getTimeInMillis()); 
			ValueFactory valueFactory = session.getValueFactory();               
			Binary contentValue = valueFactory.createBinary(is); 
			
			fileNode = parentNode.addNode(file.getName(), "nt:file");
			System.out.println("IN addVersioning - fileNode gbnerated");
			System.out.println("  IN addVersioning *************** fileNode ************** is "+fileNode);
			System.out.println("FILE NAME = "+file.getName());
			fileNode.addMixin("mix:versionable");
			fileNode.addMixin("mix:referenceable");   
			
			Node fileNodeContent = fileNode.addNode("jcr:content", "nt:resource"); 
			fileNodeContent.setProperty("jcr:mimeType", "");   
			fileNodeContent.setProperty("jcr:data", contentValue);  
			fileNodeContent.setProperty(Property.JCR_LAST_MODIFIED, lastModified);
			fileNodeContent.addMixin("mix:lastModified");
			
			session.save();      
			Version firstVersion = vManager.checkin(fileNode.getPath());
			printInfo(fileNode, vManager);
						
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}  
		
		return fileNode.getPath();
		// modify the node
		/*vManager.checkout(n.getPath());
		n.setProperty("property", "bar");
		session.save();
		vManager.checkin(n.getPath());
		printInfo(n, vManager);*/

		// restore first version
		/*vManager.restore(firstVersion, true);
		printInfo(n, vManager);*/
	}

	private static void printInfo(Node n, VersionManager vManager) throws RepositoryException {
		System.out.println(String.format("Version id: "+ vManager.getBaseVersion(n.getPath()).getIdentifier()));
	}
	
	public static List<VersionFiles> addToVersionHistory(String docName, String path) throws Exception{
		
		List<VersionFiles> files = new ArrayList<VersionFiles>();
		VersionFiles file = null;
		vManager = session.getWorkspace().getVersionManager();
		VersionHistory versionHistory = vManager.getVersionHistory(path);
		VersionIterator iterator = versionHistory.getAllVersions();
		try{
			while (iterator.hasNext()) {
				Version version = iterator.nextVersion();
				String versionName = version.getName();
				if (versionName.startsWith("jcr:")) {
					continue; // skip root version
					
				}
				Node fnode = version.getFrozenNode();
				file = new VersionFiles();
				file.setVersion(version.getName());
				file.setDocument(docName);
				file.setCreatedDate(version.getCreated().getTime());
				file.setCreatedBy(session.getUserID());
				files.add(file);
			}
			versionDAO.saveVersionHistory(file);
		}catch (PathNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return files;
	}
	
	private String viewDocument(String docname, String versionToOpen) {
		// TODO Auto-generated method stub
			
		String words = "";
		FileOutputStream openedFile;
		FileInputStream fis;
		File  f = null;
		
		if(null!=versionToOpen){
			
		try{
			vManager = session.getWorkspace().getVersionManager();
			String docNode = "node /"+docname; 
			vManager.checkout(fileNode.getPath());
			VersionHistory versionHistory = vManager.getVersionHistory(fileNode.getPath());
			Version toRead = versionHistory.getVersion(versionToOpen);
			Node content = toRead.getFrozenNode();
			Node readjcrContent = content.getNode("jcr:content");
			InputStream is = readjcrContent.getProperty("jcr:data").getBinary().getStream();
			String filename ="D:\\Sanila\\CrackVerbal\\FromRepo"+versionToOpen+".docx";
		    f = new File(filename); 
			openedFile =new FileOutputStream(f);
			byte buf[]=new byte[1024];
			int len;
			while((len=is.read(buf))>0){
			    openedFile.write(buf,0,len);
			} 
			 is.close();
			 fis = new FileInputStream(filename);
			 XWPFDocument doc = new XWPFDocument(fis);
			 List<XWPFParagraph> paras = doc.getParagraphs(); //This list will hold the paragraphs
			 XWPFWordExtractor ex = new XWPFWordExtractor(doc);  //To get the words
			 for(XWPFParagraph p : paras){  //For each paragraph we retrieved from the document
			      words += p.getText();    //Add the text we retrieve to the words string  
			 }
		    
			XWPFDocument newDoc = new XWPFDocument(); 
			XWPFParagraph para = newDoc.createParagraph();
			XWPFRun run = para.createRun();     
			run.setText(words);
			newDoc.write(new FileOutputStream(new File("D:\\Sanila\\CrackVerbal\\temp.docx")));
			f.delete();
		
		} catch (RepositoryException e)	{
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		
	}
			
	return words;

	}
	
	private void editDocument(String docName, String content) {
		// TODO Auto-generated method stub
		try{
			vManager = session.getWorkspace().getVersionManager();
			vManager.checkout(fileNode.getPath());
			// Apache POI error fix
				FileOutputStream out =null;
				FileInputStream is;
				System.out.println("IN editDocument");
		    	XWPFDocument document = new XWPFDocument(); 
				String fileName = "D:\\Sanila\\CrackVerbal\\"+docName;
			    out = new FileOutputStream(new File(fileName));
			    XWPFParagraph paragraph = document.createParagraph();
			    XWPFRun run = paragraph.createRun();
			    run.setText(content);
			    XWPFRun paragraphOneRunThree = paragraph.createRun();
			    paragraphOneRunThree.setFontSize(30);
			    document.write(out);
			    System.out.println("Document created successfully");
			      try { 
			    	  Node node = session.getNode(fileNode.getPath());
			    	  NodeIterator n =  node.getNodes("jcr:*");
			    	  while (n.hasNext()) {
			    		  	System.out.println(" NEW VERSION IS CHECKED OUT = "+n.nextNode().isCheckedOut());
			    	  		is = new FileInputStream(fileName);
			    	  		Calendar lastModified = Calendar.getInstance();   
			    	  		lastModified.setTimeInMillis(lastModified.getTimeInMillis()); 
			    	  		ValueFactory valueFactory = session.getValueFactory();               
			    	  		Binary contentValue = valueFactory.createBinary(is); 
			    	  		long pos = n.getPosition();
			    	  		Node childNode = node.getNode("jcr:content");
			    	  		childNode.setProperty("jcr:data", contentValue);
			    	  		System.out.println(" VERSION IS CHECKED OUT and UPDATED ");	  
			      	 }
			    } catch (FileNotFoundException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
			    }  
			
			session.save();
	        vManager.checkin(fileNode.getPath());
			printInfo(fileNode, vManager);
			
		}catch(Exception e){}

	}
	
	
}

